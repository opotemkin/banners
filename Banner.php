<?php

class Banner extends CApplicationComponent
{
    
    public $count;
    
    public function init()
    {
        parent::init();
    }
    
    public function showBanner($name, $per=null)
    {
        // Чтобы обнулить сессию для данного баннера - раскомментируйте и обновите страницу
//         Yii::app()->session[$name] = $this->count;
        
        if ( !isset( Yii::app()->session[$name] ) ) // Если в сессии нет, скорее всего данный шаблон еще не отображался
            Yii::app()->session[$name] = $this->count;
        
        // Если показов для данного баннера нет, то дальше не идем
        if ( Yii::app()->session[$name] == 0 )
                                    return false;
        
        // Определить директорию, в которой находятся шаблоны html/js или php (согласно ТЗ Yii)
        $dirFile = Yii::app()->request->baseUrl. 'upload/';
        
        // Определить форматы файлов шаблонов
        $fileJs = $dirFile.$name. '.js';
        $fileHtml = $dirFile.$name. '.html';
        $filePhp = $dirFile.$name. '.php';
        
        if (is_readable( $fileJs )) { // Если шаблон в js-файле
            // Разрешить или запретить выполнять js-код?
            $text = $fileJs;
            $typeFile = 'js';
        }
        
        if (is_readable( $fileHtml )) { // Если шаблон в htmls-файле
            $text = CHtml::encode( $fileHtml );
            $typeFile = 'html';
        }
        
        if (is_readable( $filePhp )) { // Если шаблон в php-файле
            $text = $filePhp;
            $typeFile = 'php';
        }
        
        $count = Yii::app()->session[$name]; // Количество повторов из конфига
        
        if ( isset($text) )
            // Вывести на экран $count*$per (% от доступного количества) раз
            $this->getCount($text, $per, $typeFile, $count, $name);
        else
            return false;
        
    }
    
    /**
     * В данной функции учитываются % и количество показов
     */
    public function getCount($text, $per=null, $typeFile, $count, $name)
    {
        // Если % задан во view-файле
        if ( $per!=NULL ) {
            $per = CHtml::encode($per);
            $value = round( $count*( (int)$per/100 ) );
            
            // убедимся что не больше 100% от возможного количества оставшихся показов
            if ( $value > $count )
                        $value = $count;
            
        } else { // Иначе считаем, что надо отобразить максимальное количество раз от оставшегося в сессии
            $value = $count;
        }
        
        // Отображаем необходимое количество раз
        for ( $i=0; $i<$value; $i++ ):
            if ( $typeFile == 'js' ) { // Если это js-шаблон, то оборачиваем его в тег script
                echo '<script>';
                require $text ;
                echo '</script>';
            } else {
                require $text ;
            }

        endfor;
        
        // Оставшиеся показы для данного баннера сохраним в сессию
        Yii::app()->session[$name] = ( $count - $value );
        
    }
    
    
}